# Example Rust Project

[![pipeline status](https://gitlab.com/dealako/rust-hello/badges/master/pipeline.svg)](https://gitlab.com/dealako/rust-hello/commits/master)
[![coverage report](https://gitlab.com/dealako/rust-hello/badges/master/coverage.svg)](https://gitlab.com/dealako/rust-hello/commits/master)

## Prerequisites

The following tools are required for building and running the examples:

* gcc
* make

## Rust Tool Installation

To install rust, simply run:

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

After installation, load the environment to setup your path.

## Setup the Environment

To setup the environment, load the environment file:

```bash
source $HOME/.cargo/env
```

## Building the Project

To build the project, run `make`.

```bash
make
```

## Running the Unit Tests

To run the unit tests, execute:

```bash
make test
```

## Running the Lint Tests

To run the lint tests, execute:

```bash
make lint
```

## Running the Formatter

To format the code, execute:

```bash
make fmt
```

## Run the Application

The application is a copy of the greatest common divisor example application published in the _Programming Rust_ book by
[O'Reilly Media](http://shop.oreilly.com/product/0636920040385.do).

To run the application, first build the example (`make`) and then:

```bash
./target/debug/rust-hello
Usage: gcd NUMBER ...
```

This will show a brief usage which indicates it is expecting one or more integer values.

```bash
./target/debug/rust-hello 5 10 25 45
The greatest common divisor of [5, 10, 25, 45] is 5
```

