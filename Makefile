# Copied from https://gitlab.com/bitemyapp/totto/blob/master/Makefile
package = rust-hello

env = PKG_CONFIG_ALLOW_CROSS=1 # OPENSSL_INCLUDE_DIR=/usr/local/opt/openssl/include
cargo = $(env) cargo
rustc = $(env) rustc
rustup = $(env) rustup
debug-env = RUST_BACKTRACE=1 RUST_LOG=$(package)=debug
debug-cargo = $(env) $(debug-env) cargo

build: # dev-deps
	$(cargo) build

version:
	@$(rustc) --version
	@$(cargo) --version

build-dynamic-release:
	$(cargo) build --release

build-static-release:
	$(cargo) build --target x86_64-unknown-linux-musl --release

clippy: #dev-deps
	$(cargo) clippy

run: build
	./target/debug/$(package)

install:
	$(cargo) install --force

test:
	$(cargo) test

test-debug:
	$(debug-cargo) test -- --nocapture

fmt: #dev-deps
	$(cargo) fmt

lint: #dev-deps
	$(cargo) clippy

watch:
	$(cargo) watch

doc:
	$(cargo) doc

clean:
	@rm -Rf target

# You need nightly for rustfmt at the moment
#dev-deps:
#	@$(rustup) component add rustfmt-preview
#	@$(rustup) component add clippy-preview

.PHONY : build version build-dynamic-release build-static-release build-dynamic-image build-static-image run install test test-debug fmt watch clean dev-deps
